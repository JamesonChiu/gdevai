﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementControls : MonoBehaviour
{
    float speed = 20;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //CONTROLS
        float Htransition = Input.GetAxis("Horizontal") * speed * Time.deltaTime; 
        this.transform.position += new Vector3(Htransition, 0, 0);


        float Vtransition = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        this.transform.position += new Vector3(0, 0, Vtransition);

        //MOUSE
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.gameObject.transform.LookAt(new Vector3(mousePos.x - transform.position.x , this.gameObject.transform.position.y, mousePos.z - transform.position.z));

    }
}
 