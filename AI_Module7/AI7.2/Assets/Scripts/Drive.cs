﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drive : MonoBehaviour {

 	public float speed = 10.0F;
    public float rotationSpeed = 100.0F;

    public GameObject bullet;
    public GameObject turret;

    public int Health = 100;

    void Update() {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);

        if(Input.GetKeyDown("space"))
        {
           GameObject PB =  Instantiate(bullet, turret.transform.position, turret.transform.rotation);
           PB.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
        }

        if (Health <= 0)
        {
            Destroy(this.gameObject);
            
        }
	}

    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.CompareTag("shells"))
        {
            Debug.Log("Player Damaged");
            Health -= 10;
        }
    }

}
