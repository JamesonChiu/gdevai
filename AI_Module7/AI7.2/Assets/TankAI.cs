﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : MonoBehaviour
{

    Animator anim;

    public GameObject player;

    public GameObject bullet;
    public GameObject turret;

    int Health;

    public GameObject GetPlayer()
    {
        return player;
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        Health = 100;
    }

    // Update is called once per frame
    void Update()
    {
       
        if (player != null)
        {
            anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
            anim.SetInteger("Enemy_HP", Health);
        }
        else
        {
            anim.SetFloat("distance", 1000);
        }
       
        if (Health <= 0)
        {
            OnDeath();
        }
    }

    void Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }
    public void StopFiring()
    {
        CancelInvoke("Fire");
    }

    public void StartFiring()
    {
        InvokeRepeating("Fire", 0.5f, 0.5f);
    }

    public void OnDeath()
    {
       
       Destroy(this.gameObject);
       
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("shells"))
        {
            Debug.Log("Enemy Damage");
            Health -= 10;
        }
    }
}
