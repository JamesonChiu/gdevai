﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovetoDirection : MonoBehaviour
{
    public Vector3 direction = new Vector3(8, 0, 4);
    float speed = 5;

    // Start is called before the first frame update
    void Start()
    {
        direction *= .01f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction.normalized*speed * Time.deltaTime);
    }
}
