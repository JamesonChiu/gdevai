﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : NPC_Base
{
   
   [SerializeField] GameObject[] waypoints;
   // [SerializeField] GameObject[] waypoints2;
    public int currentWaypoint;

    public void Awake()
    {
        waypoints = GameObject.FindGameObjectsWithTag("waypoint");
      //  waypoints2 = GameObject.FindGameObjectsWithTag("waypoint2");

    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        NPC = animator.gameObject;
        int RandomRange = Random.Range(0, waypoints.Length);
        currentWaypoint = RandomRange;
    }

   // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        base.OnStateUpdate(animator, stateInfo, layerIndex);

        if (waypoints.Length == 0) return;
        if (Vector3.Distance(waypoints[currentWaypoint].transform.position, NPC.transform.position) < accuracy)
        {
            currentWaypoint++;
            if (currentWaypoint >= waypoints.Length)
            {
                currentWaypoint = 0;
            }
        }

        var direction = waypoints[currentWaypoint].transform.position - NPC.transform.position;
        NPC.transform.rotation = Quaternion.Slerp(NPC.transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);
        NPC.transform.Translate(0, 0, speed * Time.deltaTime);
        

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }


}


/*  if (GameObject.FindGameObjectWithTag("Tank"))
          {

              if (waypoints.Length == 0) return;
              if (Vector3.Distance(waypoints[currentWaypoint].transform.position, NPC.transform.position) < accuracy)
              {
                  currentWaypoint++;
                  if (currentWaypoint >= waypoints.Length)
                  {
                      currentWaypoint = 0;
                  }
              }

              var direction = waypoints[currentWaypoint].transform.position - NPC.transform.position;
              NPC.transform.rotation = Quaternion.Slerp(NPC.transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);
              NPC.transform.Translate(0, 0, speed * Time.deltaTime);
          }

          if (GameObject.FindGameObjectWithTag("Tank2"))
          {

              if (waypoints2.Length == 0) return;
              if (Vector3.Distance(waypoints2[currentWaypoint].transform.position, NPC.transform.position) < accuracy)
              {
                  currentWaypoint++;
                  if (currentWaypoint >= waypoints2.Length)
                  {
                      currentWaypoint = 0;
                  }
              }

              var direction = waypoints2[currentWaypoint].transform.position - NPC.transform.position;
              NPC.transform.rotation = Quaternion.Slerp(NPC.transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);
              NPC.transform.Translate(0, 0, speed * Time.deltaTime);
          }*/
