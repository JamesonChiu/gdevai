﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovetoGoal : MonoBehaviour
{

    public Transform goal;
    float speed = 5;

    // Start is called before the first frame update
    void Start()
    {
        transform.LookAt(goal);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = goal.position - this.transform.position;
        transform.LookAt(goal);


        if (direction.magnitude >1)
        {
            transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);
        }

        

    }
}
