﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vechicleMovement : MonoBehaviour
{
    public Transform goal;
    public float speed = 0f;
    public float rotSpeed = 15f;
    public float acceleration = 5f;
    public float decceleration = 5f;
    public float minSpeed = 0f;
    public float maxSpeed = 10f;
    public float breakAngle = 20f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 lookAtGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);
        Vector3 direction = lookAtGoal - this.transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);

        if(Vector3.Angle(goal.forward, this.transform.forward) > breakAngle && speed > 2)
        {
            speed = Mathf.Clamp(speed - (decceleration * Time.deltaTime), minSpeed, maxSpeed);

        }
        else
        {
            speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minSpeed, maxSpeed);

        }

        this.transform.Translate(0,0,speed);
    }
}
