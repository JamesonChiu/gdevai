﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI_Control2 : MonoBehaviour
{
    NavMeshAgent agent;

    public GameObject target;

    public WASDMovement P_movement;

    Vector3 WanderTarget;

   // public Transform player;

    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        P_movement = target.GetComponent<WASDMovement>();
    }

    void Seek(Vector3 location)
    {
        agent.SetDestination(location);
    }

    void Wander()
    {
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        WanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f * wanderJitter));
        WanderTarget.Normalize();
        WanderTarget *= wanderRadius;

        Vector3 targetLocal = WanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        Seek(targetWorld);
    }

    void Hide()
    {
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;

        int hidingSpotCount = World.Instance.GetHindingSpots().Length;
        for (int i = 0; i < hidingSpotCount; i++)
        {
            Vector3 hideDirection = World.Instance.GetHindingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHindingSpots()[i].transform.position + hideDirection.normalized * 5;

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
            if (spotDistance < distance)
            {
                chosenSpot = hidePosition;
                distance = spotDistance;
            }
        }

        Seek(chosenSpot);
    }

    void CleverHide()
    {
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;
        Vector3 chosenDir = Vector3.zero;
        GameObject chosenGameObject = World.Instance.GetHindingSpots()[0];

        int hidingSpotCount = World.Instance.GetHindingSpots().Length;
        for (int i = 0; i < hidingSpotCount; i++)
        {
            Vector3 hideDirection = World.Instance.GetHindingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHindingSpots()[i].transform.position + hideDirection.normalized * 5;

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
            if (spotDistance < distance)
            {
                chosenSpot = hidePosition;
                chosenDir = hideDirection;
                chosenGameObject = World.Instance.GetHindingSpots()[i];
                distance = spotDistance;
            }
        }

        Collider hideCol = chosenGameObject.GetComponent<Collider>();
        Ray back = new Ray(chosenSpot, -chosenDir.normalized);
        RaycastHit info;
        float rayDistance = 100f;
        hideCol.Raycast(back, out info, rayDistance);


        Seek(info.point + chosenDir.normalized * 5);
    }

    bool CanSeeTarget()
    {
        RaycastHit raycastInfo;
        Vector3 rayToTarget = target.transform.position - this.transform.position;
        if (Physics.Raycast(this.transform.position, rayToTarget, out raycastInfo))
        {
            return raycastInfo.transform.gameObject.tag == "Player";
        }

        return false;
    }

    void Update()
    {
        //Vector3 lookGoal = new Vector3(player.position.x, this.transform.position.y, player.position.z);


      
        if (CanSeeTarget())
        {
            Debug.Log("Hiding");
            CleverHide();
        }
        else
        {
            Wander();
        }


    }
}
