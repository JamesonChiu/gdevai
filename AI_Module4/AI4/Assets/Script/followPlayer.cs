﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayer : MonoBehaviour
{
    public Transform player;
    float speed = 0.50f;
    float rotSpeed = 5;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 lookGoal = new Vector3(player.position.x, this.transform.position.y, player.position.z);

        Vector3 direction = lookGoal - transform.position;

        this.transform.rotation = (Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed));

        Vector3 smoothPosition = Vector3.Lerp(this.transform.position, player.position, speed * Time.deltaTime); 

        if (Vector3.Distance(lookGoal, transform.position) > 2)
        {
            this.transform.position = smoothPosition;
        }


    }
}
